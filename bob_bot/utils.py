from wit import Wit
from jobsearch import job_info_search

access_token = "HCSC74EDXBMHGCCOG6HOTLZOJ7QBYNPM"
client = Wit(access_token = access_token)
default_message = "I'm sorry, I do not understand"

def reply_generation(message_text):
    resp = client.message(message_text)
    intent = resp['intents']
    print(intent)
    if not intent:
        return "I'm sorry, I do not understand", False
    intent = resp['intents'][0]['name']
    entities = list(resp['entities'])
    print(entities)
    if ('name:name' in entities and 'Bob:Bob' not in entities) or intent == "correct_name":
        return "I'm sorry, my name is Bob. Please use Bob to address me.", False
    if intent == "about_you":
        return "Hello! My name is Bob, your friendly job bot. I make job searching easier for you. Just describe the " \
               "job you'd like, using the company, job profile and/or location details, and I'll fetch the relevant " \
               "results to you.", False
    if intent == "welcome":
        return "Hello! How may I help you?", False
    if intent == "thanks":
        return "It was nice talking to you, goodbye!", False
    if intent == "info":
        if not entities:
            return "I'm sorry but there isn't enough information provided for me to work with. "\
                    "Could you try again with more specific details in your question?", False
        return job_info_search(entities, resp), True
    return "Encountered an unknown problem, please try again later.", False
