import requests
import os
import decimal
import json

# Replace 'YOUR_API_KEY' with your actual Adzuna API key
api_key = '2fa739a63248a4cec9bc406eb22979e9'

# Base URL for the Adzuna API
base_url = 'https://api.adzuna.com/v1/api/jobs/in/search/1'

def search(role = '', company = '', location = ''):
    if role and company and location:
        params = {
            'app_id': 'eacb6e82',
            'app_key': api_key,
            'results_per_page': 5,  # Number of job listings per page
            'what': role + " " + company, # Job search keyword
            'where': location  # Job search location
        }
    elif not role and company and location:
        params = {
            'app_id': 'eacb6e82',
            'app_key': api_key,
            'results_per_page': 5,  # Number of job listings per page
            'what': company,  # Job search keyword
            'where': location  # Job search location
        }
    elif not company and role and location:
        params = {
            'app_id': 'eacb6e82',
            'app_key': api_key,
            'results_per_page': 5,  # Number of job listings per page
            'what': role,  # Job search keyword
            'where': location  # Job search location
        }
    elif not location and role and company:
        params = {
            'app_id': 'eacb6e82',
            'app_key': api_key,
            'results_per_page': 5,  # Number of job listings per page
            'what': role + " " + company
        }
    elif role:
        params = {
            'app_id': 'eacb6e82',
            'app_key': api_key,
            'results_per_page': 5,  # Number of job listings per page
            'what': role,  # Job search keyword
        }
    elif company:
        params = {
            'app_id': 'eacb6e82',
            'app_key': api_key,
            'results_per_page': 5,  # Number of job listings per page
            'what': company,  # Job search keyword
        }
    else:
        params = {
            'app_id': 'eacb6e82',
            'app_key': api_key,
            'results_per_page': 5,  # Number of job listings per page
            'where': location,  # Job search keyword
        }


    # Send GET request to the Adzuna API
    response = requests.get(base_url, params = params)

    if response.status_code == 200:

        data = response.json()
        categorized_jobs = []

        for job in data['results']:
            categorized_jobs.append({
                'Job Title': job['title'],
                'Company': job['company']['display_name'],
                'Location': job['location']['display_name'],
                'Link': job.get('redirect_url', None)
            })

        return categorized_jobs

    return "search failed"

def job_info_search(entities, resps):
    role_entity = company_entity = location_entity = ''
    print(resps)
    for entity in entities:
        print(entity)
        if entity == 'company_name:company_name':
            company_entity += resps['entities'][entity][0]['value']
        if entity == 'job_role:job_role' or entity == 'job_type:job_type':
            role_entity += resps['entities'][entity][0]['value'] if not role_entity else ' ' + resps['entities'][entity][0]['value']
        if entity == 'wit$location:location':
            location_entity += resps['entities'][entity][0]['resolved']['values'][0]['name']
    results = search(role_entity, company_entity, location_entity)
    print(results)
    elements = []
    for result in results:
         element = {'text': "{}\n{}\n{}".format(result['Job Title'], result['Company'], result['Location']),
                    'buttons': [{
                    'type': "web_url",
                    'url': result['Link'],
                    'title': "Apply Here",
                    'webview_height_ratio': "full"
                }]}
         elements.append(element)
    return elements