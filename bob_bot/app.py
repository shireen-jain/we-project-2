import os, sys
from flask import Flask, request
import requests
from pymessenger import Bot
from utils import reply_generation

app = Flask(__name__)
PAGE_ID = "101624649690878"
PAGE_ACCESS_TOKEN = "EAAOLyARBLBQBAGbB9UqLozPrEZBI3n6K4BlNkZAV5ypMvrIsufSucIMjTVedoSVabwuIIZBWTaCtRvVZBlmUZCgan9blnAHCnQBB2q5AM2vTZB8J1BOPxCLJB69L2kb788vnVVbBilufGZCRe57ktLCydUKGF5ZBdFDfNWeZB98Dx8ppeWWe8rVNSVZBjZApUs262YZD"
bot = Bot(PAGE_ACCESS_TOKEN)

@app.route('/', methods = ['GET'])
def verify():
    if request.args.get("hub.mode") == "subscribe" and request.args.get("hub.challenge"):
        if not request.args.get("hub.verify_token") == "hello":
            return "Verification token mismatch", 403
        return request.args["hub.challenge"], 200
    return "Gateway to the Bot", 200

@app.route('/', methods = ['POST'])
def webhook():
    data = request.get_json()
    log(data)

    if data['object'] == 'page':
        for entry in data['entry']:
            for messaging_event in entry['messaging']:
                sender_id = messaging_event['sender']['id']
                recipient_id = messaging_event['recipient']['id']
                page_id = entry['id']
                if sender_id != page_id:
                    send_typing_indicator(sender_id)

                    if messaging_event.get('message'):
                        if 'text' in messaging_event['message']:
                            messaging_text = messaging_event['message']['text']
                        else:
                            messaging_text = None
                        response, is_button = reply_generation(messaging_text)
                        if is_button:
                            bot.send_text_message(sender_id, "Sure thing! Let me see what I can find.")
                            for reply in response:
                                print(reply)
                                bot.send_button_message(sender_id, reply['text'], reply['buttons'])
                            bot.send_text_message(sender_id, "Here are the results you asked for. Can I help you with anything else?")
                        else:
                            bot.send_text_message(sender_id, response)
                    stop_typing_indicator(sender_id)

    return "ok", 200

def send_typing_indicator(recipient_id):
    data = {
        'recipient': {'id': recipient_id},
        'sender_action': 'typing_on'
    }
    response = requests.post('https://graph.facebook.com/v12.0/me/messages',
                             params = {'access_token': PAGE_ACCESS_TOKEN},
                             json = data)
    return response.json()

def stop_typing_indicator(recipient_id):
    data = {
        'recipient': {'id': recipient_id},
        'sender_action': 'typing_off'
    }
    response = requests.post('https://graph.facebook.com/v12.0/me/messages',
                             params = {'access_token': PAGE_ACCESS_TOKEN},
                             json = data)
    return response.json()

def send_b_message(recipient_id, message, buttons=None):
    data = {
        'recipient': {'id': recipient_id},
        'message': {'text': message}
    }

    if buttons:
        data['message']['attachment'] = {
            'type': 'template',
            'payload': {
                'template_type': 'button',
                'text': message,
                'buttons': buttons
            }
        }

    response = requests.post('https://graph.facebook.com/v12.0/me/messages',
                             params={'access_token': PAGE_ACCESS_TOKEN},
                             json=data)
    return response.json()

def log(message):
    print(message)
    sys.stdout.flush()

if __name__ == "__main__":
    app.run(debug = True, port = 80)
