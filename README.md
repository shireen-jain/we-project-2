# Bob: The Job Bot - Streamlining Job Search

![Demo_-_Bob_the_Job_Bot](/uploads/aa5b371cfaf7b7d528ada8246c1d4ca7/Demo_-_Bob_the_Job_Bot.mp4)

## Overview

Bob is an AI-powered chatbot that aims to streamline the job search process by providing users with relevant and tailored job listings based on their specific requirements. Leveraging cutting-edge Natural Language Processing (NLP) techniques and API integration, JobWise offers a user-friendly interface for efficient job hunting.

## Table of Contents

- [Features](#features)
- [Prerequisites](#prerequisites)
- [Technologies Used](#technologies-used)

## Features

- Intelligent NLP for understanding user intent and job search queries in a conversational manner.
- Integration with the Adzuna API for retrieving comprehensive and real-time job listings.
- Seamless data storage and retrieval using DynamoDB, ensuring scalability and performance.
- User privacy and data security measures, adhering to ethical AI development practices.

## Prerequisites

- Python 3.8 or higher
- Access to Adzuna API key
- Facebook Messenger account for chatbot deployment

## Technologies Used

- Wit.ai for Natural Language Processing and language understanding.
- Adzuna API for fetching comprehensive and real-time job listings.
- Flask for building the web server and handling HTTP requests.
- DynamoDB for efficient data storage and retrieval.
