import requests
import boto3
import os
import decimal
import json
from botocore.exceptions import NoCredentialsError

# Replace 'YOUR_API_KEY' with your actual Adzuna API key
api_key = ''

# Replace 'YOUR_ACCESS_KEY' and 'YOUR_SECRET_KEY' with your AWS credentials
aws_access_key = 'AKIA2OYSIIRMNYAQBQPN'
aws_secret_key = '8GP2DK0zUV62cMzbIWI0wccGjTJ6N2gfMQg9N0De'
aws_region = 'ap-south-1'  # Replace with your desired AWS region

# Base URL for the Adzuna API
base_url = 'https://api.adzuna.com/v1/api/jobs/in/search/1'

# Parameters for the job search request
params = {
    'app_id': 'eacb6e82',  # Replace 'YOUR_APP_ID' with your Adzuna app ID (optional)
    'app_key': api_key,
    'results_per_page': 15,  # Number of job listings per page
    'what': 'python',  # Job search keyword
    'what': 'google',  # Job search keyword
    'where': 'india'  # Job search location
}

# Send GET request to the Adzuna API
response = requests.get(base_url, params=params)

# Check if the request was successful
if response.status_code == 200:
    # Parse the JSON response
    data = response.json()

    categorized_jobs = {}

    for job in data['results']:
        category = job['category']['label']

        if category not in categorized_jobs:
            categorized_jobs[category] = []

        categorized_jobs[category].append({
            'Job Title': job['title'],
            'Company': job['company']['display_name'],
            'Location': job['location']['display_name'],
            'Salary': job.get('salary', 'N/A'),
            'Job Description': job['description']
        })

import uuid

# ... (previous code remains unchanged)

# Create a DynamoDB resource
dynamodb = boto3.resource('dynamodb', aws_access_key_id=aws_access_key, aws_secret_access_key=aws_secret_key,
                          region_name=aws_region)

# Define the table name for storing job data
table_name = 'JobData2'

# Get or create the DynamoDB table
table = dynamodb.Table(table_name)

with table.batch_writer() as batch:
    for category, jobs in categorized_jobs.items():
        for job in jobs:
            # Generate a unique identifier (UUID) as part of the primary key
            unique_id = str(uuid.uuid4())
            item_key = f"{job['Job Description']}_{job['Location']}_{unique_id}"

            # Check if an item with the same primary key already exists in the table
            response = table.get_item(Key={'PK': item_key})

            if 'Item' not in response:
                job_item = {
                    'PK': item_key,  # Use the combination of 'Job Description', 'Location', and UUID as primary key
                    'Category': category,
                    'JobTitle': job['Job Title'],
                    'Company': job['Company'],
                    'Salary': job['Salary'],
                    'JobDescription': job['Job Description'],
                    'Location': job['Location']
                }
                batch.put_item(Item=job_item)

print('Job data stored in DynamoDB table:', table_name)